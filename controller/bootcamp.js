const fs = require('fs')
const Employee = require('./employee')
class BootCamp { 
//release 0
    static register(req, res) {
        fs.readFile('data.json', (err, data) => {
          if (err) {
            res.status(400).json({"errors": "error membaca data"})
          }else {
            let existingData = JSON.parse(data)
            let {name,password,role} = req.body
            let employee = new Employee(name,password,role)
            existingData.push(employee)
            
            fs.writeFile('data.json',JSON.stringify(existingData),(err)=>{
                if (err) {
                    res.status(400).json({errors : "error menyimpan data" })
                } else {
                    res.status(201).json({message : "berhasil register"})
                }
            })
            
        }
    })
}

    
}
module.exports = BootCamp