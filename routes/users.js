var express = require('express');
var router = express.Router();
const BootCamp = require('../controller/bootcamp')


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/:id', (req, res) => {
  console.log("parameter :", req.params.id)
  res.send(`Hello ${req.params.id} user`)
})
router.post('/register', BootCamp.register)
module.exports = router;
